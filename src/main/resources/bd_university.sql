create schema if not exists university default character set utf8 ;
use university ;

drop table if exists 'students' (
id int auto_increment primary key,
name_surname char(20) not null,
autobio varchar(100) not null,
year_of_entry date not null,
yeat_of_birth date not null,
home_address text(30) not null)
ENGINE InnoDB;

drop table if exists  'groups' (
id int auto_increment primary key,
group_cipher int,
specialty text(30) not null)
ENGINE InnoDB;

drop table if exists  'progress' (
id int auto_increment primary key,
rating int,
module_1 int,
module_2 int,
mark_100 varchar(10),
mark_5 varchar(10),
bursary int,
specialty text(30) not null)
ENGINE InnoDB;


drop table if exists 'schedule' (
id int,
semester int,
lecturer text(30) not null,
subjects text(30) not null )
ENGINE InnoDB;

insert into students (id, name_surname,autobio, year_of_entry, yeat_of_birth, home_address)
values (1, 'Ira Vaskiv', 'fistTimeStudent', '2013-09-01', '1996-08-03', 'Scrynnykastr');


insert into students (id, name_surname,autobio, year_of_entry, yeat_of_birth, home_address)
values (2, 'Solomia Bohutska', 'fistTimeStudent', '2013-09-01', '1997-11-03', 'Mykolaychyka'),
       (3, 'Mariana Bodnak', 'fistTimeStudent', '2015-09-01', '1996-08-03', 'Scrynnykastr'),
       (4, 'Yulia Nanchuk', 'fistTimeStudent', '2015-09-01', '1994-02-03', 'Scrynnykastr'),
       (5, 'Diana Sahan', 'fistTimeStudent', '2014-09-01', '1996-05-04', 'Naykova'),
       (6, 'Olia Kiyko', 'fistTimeStudent', '2011-09-01', '19964-07-03', 'Shyvar');
insert into progress (id, rating, module_1, module_2, mark_100, mark_5, bursary, specialty)
values (45, 3, 5, 4, '65','3', 11, 'Finance');

insert into progress (id, rating, module_1, module_2, mark_100, mark_5, bursary, specialty)
values (46, 3, 4, 3, '73','4', 22, 'Finance'),
      (47, 4, 4, 5, '81','4', 11, 'Finance'),
	  (48, 5, 4, 4, '90','5', 22, 'Finance'),
      (49, 3, 4, 3, '78','4', 22, 'Finance'),
      (50, 3, 4, 4, '85','5', 11, 'Finance'),
	  (51, 5, 4, 5, '73','4', 22, 'Finance');
            
insert into schedule (id,semester,lecturer,subjects)  
values (1, 3,'Boyko', 'Credit'),
       (2, 5,'Nayko', 'Math'),
       (3, 6,'Vandych', 'Auditors'),
       (4, 7,'Sahan', 'Credit');
     
insert into groups (id, group_cipher, specialty)
values (7,123,'Finance'),
       (8,126,'Finance1'),
       (9,124,'Finance2'),
       (10,125,'Finance3'),
	   (11,127,'Finance4'),
       (12,121,'Finance5'); 