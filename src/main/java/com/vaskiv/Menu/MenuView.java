package com.vaskiv.Menu;

import com.sun.corba.se.spi.orbutil.fsm.Input;
import com.vaskiv.tables.*;
import org.apache.logging.log4j.LogManager;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

public class MenuView {
    private Map<String, String> menu;
    public Map<String, Printable> methodsMenu;

    private MenuView() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Take Full Structure of DB (metadata)");
        menu.put("2", " 2 - Select All Tables");
        menu.put("3", " 3 - Select Table Student");
        menu.put("4", " 4 - Select Table Groups");
        menu.put("5", " 5 - Select Table Progress");
        menu.put("6", " 6 - Select Table Schedule");

        menu.put("31", " 31 - Take Full Structure of DB (metadata)");
        menu.put("32", " 32 - Find all Student");
        menu.put("33", " 33 - Find by Id from Student");
        menu.put("34", " 34 - create Student");
        menu.put("35", " 35 - update Student");

        menu.put("41", " 41 - Take Full Structure of DB (metadata)");
        menu.put("42", " 42 - Find all Groups");
        menu.put("43", " 43 - Find by Id from Groups");
        menu.put("44", " 44 - create Groups");
        menu.put("45", " 45 - update Groups");

        menu.put("51", " 51 - Take Full Structure of DB (metadata)");
        menu.put("52", " 52 - Find all Progress");
        menu.put("53", " 53 - Find by Id from Progress");
        menu.put("54", " 54 - create Progress");
        menu.put("55", " 55 - update Progress");

        menu.put("61", " 61 - Take Full Structure of DB (metadata)");
        menu.put("62", " 62 - Find all Schedule");
        menu.put("63", " 63 - Find by Id from Schedule");
        menu.put("64", " 64 - create Schedule");
        menu.put("65", " 65 - update Schedule");

        menu.put("0", " 0 - Exit.");

       //methodsMenu.put("1", AllTablesView::takeStructureOfDB);
        methodsMenu.put("2", new AllTablesView()::selectAllTable);
        methodsMenu.put("3", new AllTablesView()::selectStudents);
        methodsMenu.put("4", new AllTablesView()::selectGroups);
        methodsMenu.put("5", new AllTablesView()::selectProgress);
        methodsMenu.put("6", new AllTablesView()::selectSchedule);

        methodsMenu.put("31", new StudentsView()::findByAllStudents);
        methodsMenu.put("32", new StudentsView()::findStudentsByID);
        methodsMenu.put("33", new StudentsView()::createStudents);
        methodsMenu.put("34", new StudentsView()::updateStudents);

        methodsMenu.put("41", new GroupsView()::findByAllGroups);
        methodsMenu.put("42", new GroupsView()::findGroupByID);
        methodsMenu.put("43", new GroupsView()::createGroups);
        methodsMenu.put("44", new GroupsView()::updateGroups);

        methodsMenu.put("51", new ProgressView()::findByAllProgress);
        methodsMenu.put("52", new ProgressView()::findProgressByID);
        methodsMenu.put("53", new ProgressView()::createProgress);
        methodsMenu.put("54", new ProgressView()::updateProgress);

        methodsMenu.put("61", new ScheduleView()::findByAllSchedule);
        methodsMenu.put("62", new ScheduleView()::findScheduleByID);
        methodsMenu.put("63", new ScheduleView()::createSchedule);
        methodsMenu.put("64", new ScheduleView()::updateSchedule);

        methodsMenu.put("0", this ::exitFromProgram);
    }

     private void exitFromProgram() {
         Logger log = (Logger) LogManager.getLogger("SUCCESSFULLY END");
        System.exit(0);
    }

    private void outputMenu() {
        Logger log = (Logger) LogManager.getLogger("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public static void runMenu () {
        String key;
        MenuView menu = new MenuView();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Please, select a Point");
                System.out.println("\n\n");
                } catch (NullPointerException e) {
                Logger log = (Logger) LogManager.getLogger("\nChoose correct choice please");
            } catch (Exception e) {
                Logger log = (Logger) LogManager.getLogger("ERROR!!!");
                e.printStackTrace();
            }
        }
    }
}
