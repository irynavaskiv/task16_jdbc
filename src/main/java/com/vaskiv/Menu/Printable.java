package com.vaskiv.Menu;

import java.sql.SQLException;

@FunctionalInterface
public interface Printable {
    void print() throws SQLException,ClassNotFoundException;
}
