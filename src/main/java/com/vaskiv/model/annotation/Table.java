package com.vaskiv.model.annotation;

public @interface Table {
    String name();
}
