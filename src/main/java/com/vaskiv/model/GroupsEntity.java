package com.vaskiv.model;

import com.vaskiv.model.annotation.Column;
import com.vaskiv.model.annotation.PrimaryKey;
import com.vaskiv.model.annotation.Table;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

  @Table(name = "groups")
public class GroupsEntity implements List<GroupsEntity> {
    @PrimaryKey
    @Column(name = "id",length = 30 )
    private Integer id;
    @Column(name = "group_cipher", length = 30)
    private String group_cipher;
    @Column(name = "specialty", length = 30)
    private String specialty;

        public GroupsEntity(Integer id, String group_cipher, String specialty){
        this.id = id;
        this.group_cipher=group_cipher;
        this.specialty=specialty;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroup_cipher() {
        return group_cipher;
    }

    public void setGroup_cipher(String group_cipher) {
        this.group_cipher = group_cipher;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<GroupsEntity> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean add(GroupsEntity groupsEntity) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends GroupsEntity> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends GroupsEntity> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {

    }
    @Override
    public GroupsEntity get(int i) {
        return null;
    }

    @Override
    public GroupsEntity set(int i, GroupsEntity groupsEntity) {
        return null;
    }

    @Override
    public void add(int i, GroupsEntity groupsEntity) {
    }
    @Override
    public GroupsEntity remove(int i) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<GroupsEntity> listIterator() {
        return null;
    }

    @Override
    public ListIterator<GroupsEntity> listIterator(int i) {
        return null;
    }

    @Override
    public List<GroupsEntity> subList(int i, int i1) {
        return null;
    }

      public int getId() {
            return id;
      }
  }
