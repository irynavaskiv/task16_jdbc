package com.vaskiv.model;

import com.vaskiv.model.annotation.Column;
import com.vaskiv.model.annotation.PrimaryKey;
import com.vaskiv.model.annotation.Table;

public class ProgressEntity {
    @Table(name = "progress")
    @PrimaryKey
    @Column(name = "id", length = 30)
    private String id;
    @Column(name = "rating", length = 30)
    private String rating;
    @Column(name = "module_1", length = 30)
    private String module_1;
    @Column(name = "module_2", length = 30)
    private String module_2;
    @Column(name = "mark_100", length = 30)
    private String mark_100;
    @Column(name = "mark_5", length = 30)
    private String mark_5;
    @Column(name = "bursary", length = 30)
    private String bursary;
    @Column(name = "speciality", length = 30)
    private String speciality;

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public ProgressEntity() {
        this.id = id;
        this.rating = rating;
        this.module_1 = module_1;
        this.module_2 = module_2;
        this.mark_100 = mark_100;
        this.mark_5 = mark_5;
        this.bursary = bursary;
        this.speciality = speciality;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getModule_1() {
        return module_1;
    }

    public void setModule_1(String module_1) {
        this.module_1 = module_1;
    }

    public String getModule_2() {
        return module_2;
    }

    public void setModule_2(String module_2) {
        this.module_2 = module_2;
    }

    public String getMark_100() {
        return mark_100;
    }

    public void setMark_100(String mark_100) {
        this.mark_100 = mark_100;
    }

    public String getMark_5() {
        return mark_5;
    }

    public void setMark_5(String mark_5) {
        this.mark_5 = mark_5;
    }

    public String getBursary() {
        return bursary;
    }

    public void setBursary(String bursary) {
        this.bursary = bursary;
    }
}

