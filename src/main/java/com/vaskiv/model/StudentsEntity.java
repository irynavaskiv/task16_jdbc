package com.vaskiv.model;

import com.vaskiv.model.annotation.Column;
import com.vaskiv.model.annotation.PrimaryKey;
import com.vaskiv.model.annotation.Table;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class StudentsEntity {
        @Table(name = "students")
        @PrimaryKey
        @Column(name = "id", length = 30)
        private String id;
        @Column(name = "name_surname", length = 20)
        private String name_surname;
        @Column(name = "autobio", length = 100)
        private String autobio;
        @Column(name = "year_of_entry", length = 30)
        private String year_of_entry;
        @Column(name = "yeat_of_birth", length = 30)
        private String yeat_of_birth;
        @Column(name = "home_address", length = 30)
        private String home_address;

        public StudentsEntity (String id, String name_surname, String autobio, String yeat_of_birth, String year_of_entry, String home_address){
            this.id = id;
            this.name_surname = name_surname;
            this.autobio = autobio;
            this.yeat_of_birth= yeat_of_birth;
            this.year_of_entry=year_of_entry;
            this.home_address=home_address;
        }
        public StudentsEntity(Integer id, Integer name_surname, Integer autobio, Integer yearth_of_entry, Integer yeat_of_birth, Integer home_address){

        }
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName_surname() {
            return name_surname;
        }

        public void setName_surname(String name_surname) {
            this.name_surname = name_surname;
        }

        public String getAutobio() {
            return autobio;
        }

        public void setAutobio(String autobio) {
            this.autobio = autobio;
        }

        public String getYear_of_entry() {
            return year_of_entry;
        }

        public void setYear_of_entry(String year_of_entry) {
            this.year_of_entry = year_of_entry;
        }

        public String getYeat_of_birth() {
            return yeat_of_birth;
        }

        public void setYeat_of_birth(String yeat_of_birth) {
            this.yeat_of_birth = yeat_of_birth;
        }

        public String getHome_address() {
            return home_address;
        }

        public void setHome_address(String home_address) {
            this.home_address = home_address;
        }
}


