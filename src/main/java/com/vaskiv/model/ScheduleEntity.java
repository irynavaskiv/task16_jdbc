package com.vaskiv.model;

import com.vaskiv.model.annotation.Column;
import com.vaskiv.model.annotation.PrimaryKey;
import com.vaskiv.model.annotation.Table;

@Table(name = "schedule")
public class ScheduleEntity {
    @PrimaryKey
    @Column(name = "id", length = 30)
    private Integer id;
    @Column(name = "semester", length = 30)
    private String semester;
    @Column(name = "lecturer", length = 30)
    private String lecturer;
    @Column(name = "subjects", length = 30)
    private String subjects;

    public ScheduleEntity(Integer id, String semester, String lecturer, String subjects) {
        this.id = id;
        this.semester = semester;
        this.lecturer = lecturer;
        this.subjects = subjects;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }
}
