package com.vaskiv.service;

import com.vaskiv.DAO.implementation.StudentsDaoImpl;
import com.vaskiv.model.StudentsEntity;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class StudentsService {
    public List<StudentsEntity> findAll() throws SQLException {
        return new StudentsDaoImpl().findAll();
    }

    public static StudentsEntity findById(String id) throws SQLException {
        return new StudentsDaoImpl().findById(id);
    }

    public static int create(StudentsEntity entity) throws SQLException {
       return new StudentsDaoImpl().create(entity);
    }

    public int update (StudentsEntity entity) throws SQLException {
        return new StudentsDaoImpl().update(entity);
    }

    public static int delete(String id) throws SQLException {
        return new StudentsDaoImpl().delete(id);
    }

    public int deleteMethod(String idDeleted, String idMoveTo) throws SQLException {
        int deleteThing = 0;
        Connection connection = com.vaskiv.persistant.Connection.getConnection();
        {
            try {
                connection.setAutoCommit(false);
                if (new StudentsDaoImpl().findById(idMoveTo) == null)
                    throw new SQLException();

                List<StudentsEntity> students = new StudentsDaoImpl().findbyId(idDeleted);
                for (StudentsEntity entity : students) {
                    entity.setId(idMoveTo);
                    new StudentsDaoImpl().update(entity);
                }
                deleteThing = new StudentsDaoImpl().delete(idDeleted);
                connection.commit();
            } catch (SQLException e) {
                if (connection != null){
                    System.out.println("Transaction is roll back!");
                    connection.rollback();
                }
            } finally {
                connection.setAutoCommit(true);
            }
        }
        return deleteThing;
    }
}