package com.vaskiv.service;

import com.vaskiv.DAO.implementation.GroupsDaoImpl;
import com.vaskiv.model.GroupsEntity;

import java.sql.SQLException;
import java.util.List;

public class GroupsService {
    public static List<GroupsEntity> findAll() throws SQLException{
        return new GroupsDaoImpl().findAll();
    }
    public GroupsEntity findById (String id) throws SQLException{
        return (GroupsEntity) new GroupsDaoImpl().findById(id);
    }
    public static int create(GroupsEntity entity)throws SQLException {
        return  new GroupsDaoImpl().create(entity);
    }
    public int update (GroupsEntity entity)throws SQLException {
        return  new GroupsDaoImpl().update(entity);
    }
    public static int delete(String id)throws SQLException {
        return  new GroupsDaoImpl().delete(id);
    }
}