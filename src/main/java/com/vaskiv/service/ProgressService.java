package com.vaskiv.service;

import com.vaskiv.DAO.implementation.GroupsDaoImpl;
import com.vaskiv.DAO.implementation.ProgressDaoImpl;
import com.vaskiv.model.GroupsEntity;
import com.vaskiv.model.ProgressEntity;

import java.sql.SQLException;
import java.util.List;

public class ProgressService {
    public List<ProgressEntity> findAll () throws SQLException {
        return new ProgressDaoImpl().findAll();
    }
    public ProgressEntity findById (String id) throws SQLException{
        return new ProgressDaoImpl().findById(id);
    }
    public static int create(ProgressEntity entity)throws SQLException {
        return  new ProgressDaoImpl().create(entity);
    }
    public int update (ProgressEntity entity)throws SQLException {
        return  new ProgressDaoImpl().update(entity);
    }
    public static int delete(String id)throws SQLException {
        return  new ProgressDaoImpl().delete(id);
    }
}
