package com.vaskiv.service;

import com.vaskiv.DAO.implementation.ScheduleDaoImpl;
import com.vaskiv.model.ScheduleEntity;

import java.sql.SQLException;
import java.util.List;

public class ScheduleService {
    public List<ScheduleEntity> findAll () throws SQLException {
        return new ScheduleDaoImpl().findAll();
    }
    public static List<ScheduleEntity> findById(String id) throws SQLException{
        return new ScheduleDaoImpl().findById(id);
    }
    public static int create(ScheduleEntity entity)throws SQLException {
        return  new ScheduleDaoImpl().create(entity);
    }
    public static int update(ScheduleEntity entity)throws SQLException {
        return  new ScheduleDaoImpl().update(entity);
    }
    public static int delete(String id)throws SQLException {
        return  new ScheduleDaoImpl().delete(id);
    }
}
