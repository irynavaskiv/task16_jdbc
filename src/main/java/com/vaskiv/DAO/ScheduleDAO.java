package com.vaskiv.DAO;

import com.vaskiv.DAO.GeneralDAO;
import com.vaskiv.model.StudentsEntity;

public interface ScheduleDAO extends GeneralDAO<StudentsEntity, String> {
}
