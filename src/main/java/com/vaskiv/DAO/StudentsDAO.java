package com.vaskiv.DAO;

import com.vaskiv.model.StudentsEntity;

import java.sql.SQLException;
import java.util.List;

public interface StudentsDAO extends GeneralDAO<StudentsEntity, String> {
    List<StudentsEntity> findByName (String name) throws SQLException;
    List<StudentsEntity> findbyId (String id) throws SQLException;
   }
