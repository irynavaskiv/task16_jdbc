package com.vaskiv.DAO.implementation;

import com.vaskiv.model.GroupsEntity;
import com.vaskiv.persistant.Connection;
import com.vaskiv.transformer.transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GroupsDaoImpl {
    private static final String FIND_ALL = "select * from groups";
    private static final String FIND_BY_ID = "select * from groups where id = ?";
    private static final String DELETE = "delete from groups where id=2";
    private static final String CREATE = "insert into groups (id,group_cipher,specialty) values (?,?,?)";
    private static final String UPDATE = "update groups set id=?,group_cipher=?,specialty=? where id = ?";



    public static List<GroupsEntity> findAll() throws SQLException {
        List<GroupsEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    groups.add((GroupsEntity) new transformer(GroupsEntity.class).fromResultSetToEntity(resultSet));
                }
            }
            return groups;
        }
    }

    public static List<GroupsEntity> findById(String id) throws SQLException {
        GroupsEntity entity = null;
        List<GroupsEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (GroupsEntity) new transformer(GroupsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    public static int create(GroupsEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(1, entity.getGroup_cipher());
            ps.setString(1, entity.getSpecialty());
            return ps.executeUpdate();
        }
    }

    public static int update(GroupsEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(1, entity.getGroup_cipher());
            ps.setString(1, entity.getSpecialty());
            return ps.executeUpdate();

        }
    }

    public static int delete(String id) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1,id);
            return ps.executeUpdate();

        }
    }
 }