package com.vaskiv.DAO.implementation;

import com.vaskiv.DAO.StudentsDAO;
import com.vaskiv.model.StudentsEntity;
import com.vaskiv.persistant.Connection;
import com.vaskiv.transformer.transformer;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentsDaoImpl implements StudentsDAO {
    private static final String FIND_ALL = "select * from students";
    private static final String DELETE = "delete from students where id=2";
    private static final String CREATE = "insert into students (id,name_surname) values (?,?)";
    private static final String UPDATE = "update students set id=?, name_surname=?,autobio=? where id = ?";
    private static final String FIND_BY_ID = "select * from students where id = ?";

    public int create (StudentsEntity entity) throws SQLException{
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement (CREATE)){
            ps.setString(1, entity.getHome_address());
            ps.setString(1, entity.getAutobio());
            ps.setString(1, entity.getId());
            ps.setString(1, entity.getName_surname());
            ps.setString(1, entity.getYear_of_entry());
            ps.setString(1, entity.getYeat_of_birth());
            return ps.executeUpdate();
        }
    }
    public int update (StudentsEntity entity) throws SQLException{
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)){
               ps.setString(1,entity.getHome_address());
               ps.setString(1,entity.getAutobio());
               ps.setString(1,entity.getId());
               ps.setString(1,entity.getName_surname());
               ps.setString(1,entity.getYear_of_entry());
               ps.setString(1,entity.getYeat_of_birth());
               return ps.executeUpdate();
        }
    }
    public int delete (String id) throws SQLException {
           java.sql.Connection conn = Connection.getConnection();
           try (PreparedStatement ps = conn.prepareStatement (DELETE)){
             ps.setString(1, id);
             return ps.executeUpdate();
           }
         }

    public List<StudentsEntity> findAll(List<StudentsEntity> entity) throws SQLException{
        List<StudentsEntity> students  = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
          try (Statement statement = connection.createStatement ()){
              try (ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                  while (resultSet.next ()) {
                      students.add((StudentsEntity) new transformer(StudentsEntity.class).fromResultSetToEntity(resultSet));
                  }
             }
          }
        return entity;
    }

    public StudentsEntity findById(String id) throws SQLException {
        StudentsEntity entity = null;
        java.sql.Connection connection = Connection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID))  {
            ps.setInt(1, Integer.parseInt(id));
            try (ResultSet resultSet =ps.executeQuery()){
                while (resultSet.next()){
                    entity=(StudentsEntity) new transformer(StudentsEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public List<StudentsEntity> findByName(String name) throws SQLException {
        return null;
    }

    @Override
    public List<StudentsEntity> findbyId(String id) throws SQLException {
        return null;
    }

    public List<StudentsEntity> findAll() throws SQLException {
        return null;
    }
}