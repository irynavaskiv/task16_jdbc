package com.vaskiv.DAO.implementation;

import com.vaskiv.DAO.ProgressDAO;
import com.vaskiv.model.ProgressEntity;
import com.vaskiv.persistant.Connection;
import com.vaskiv.transformer.transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProgressDaoImpl  {
    private static final String FIND_ALL = "select * from progress ";
    private static final String FIND_BY_ID = "select * from progress where id = ?";
    private static final String DELETE = "delete from progress where id=?";
    private static final String CREATE = "insert into progress (id,rating,module_1,module_2,mark_100,mark_5,bursary,specialty) values(?,?,?,?,?,?,?,?)";
    private static final String UPDATE = "update progress set id=?,rating=?,module_1=? module_2=?, mark_100=?,mark_5=?,bursary=?,specialty=? where id = ?";

    public List<ProgressEntity> findAll() throws SQLException {
        List<ProgressEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    groups.add((ProgressEntity) new transformer(ProgressEntity.class).fromResultSetToEntity(resultSet));
                }
            }
            return groups;
        }
    }

    public ProgressEntity findById(String id) throws SQLException {
        ProgressEntity entity = null;
        List<ProgressEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ProgressEntity) new transformer(ProgressEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    public int create(ProgressEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setString(1, entity.getId());
            ps.setString(1, entity.getRating());
            ps.setString(1, entity.getModule_1());
            ps.setString(1, entity.getModule_1());
            ps.setString(1, entity.getModule_2());
            ps.setString(1, entity.getMark_100());
            ps.setString(1, entity.getMark_5());
            ps.setString(1, entity.getSpeciality());
            ps.setString(1, entity.getBursary());
            return ps.executeUpdate();
        }
    }

    public int update(ProgressEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getId());
            ps.setString(1, entity.getRating());
            ps.setString(1, entity.getModule_1());
            ps.setString(1, entity.getModule_1());
            ps.setString(1, entity.getModule_2());
            ps.setString(1, entity.getMark_100());
            ps.setString(1, entity.getMark_5());
            ps.setString(1, entity.getSpeciality());
            ps.setString(1, entity.getBursary());
            return ps.executeUpdate();

        }
    }

    public int delete (String id) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1,id);
            return ps.executeUpdate();

        }
    }
}

