package com.vaskiv.DAO.implementation;

import com.vaskiv.model.ScheduleEntity;
import com.vaskiv.persistant.Connection;
import com.vaskiv.transformer.transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ScheduleDaoImpl {
    private static final String FIND_ALL = "select * from schedule ";
    private static final String FIND_BY_ID = "select * from schedule where id = ?";
    private static final String DELETE = "delete from schedule where id=?";
    private static final String CREATE = "insert into schedule (id,semester,lecturer,subject) values(?,?,?,?)";
    private static final String UPDATE = "update schedule set id=?,semester=?,lecturer=? subject=? where id = ?";

       public List<ScheduleEntity> findAll() throws SQLException {
        List<ScheduleEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    groups.add((ScheduleEntity) new transformer(ScheduleEntity.class).fromResultSetToEntity(resultSet));
                }
            }
            return groups;
        }
    }

    public List<ScheduleEntity> findById(String id) throws SQLException {
        ScheduleEntity entity = null;
        List<ScheduleEntity> groups = new ArrayList<>();
        java.sql.Connection connection = Connection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (ScheduleEntity) new transformer(ScheduleEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return (List<ScheduleEntity>) entity;
    }

    public int create(ScheduleEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(1, entity.getSemester());
            ps.setString(1, entity.getLecturer());
            ps.setString(1, entity.getSubjects());
            return ps.executeUpdate();
        }
    }

    public int update(ScheduleEntity entity) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(1, entity.getSemester());
            ps.setString(1, entity.getLecturer());
            ps.setString(1, entity.getSubjects());
            return ps.executeUpdate();

        }
    }

    public int delete (String id) throws SQLException {
        java.sql.Connection conn = Connection.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1,id);
            return ps.executeUpdate();

        }
    }
}
