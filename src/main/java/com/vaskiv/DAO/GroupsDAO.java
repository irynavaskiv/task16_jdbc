package com.vaskiv.DAO;

import com.vaskiv.DAO.GeneralDAO;
import com.vaskiv.model.StudentsEntity;

public interface GroupsDAO extends GeneralDAO<StudentsEntity, String> {
}
