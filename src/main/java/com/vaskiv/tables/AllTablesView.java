package com.vaskiv.tables;

import com.mysql.cj.jdbc.ConnectionGroup;
import com.mysql.cj.jdbc.ConnectionGroupManager;
import com.vaskiv.DAO.implementation.GroupsDaoImpl;
import com.vaskiv.DAO.implementation.ProgressDaoImpl;
import com.vaskiv.DAO.implementation.ScheduleDaoImpl;
import com.vaskiv.DAO.implementation.StudentsDaoImpl;
import com.vaskiv.model.GroupsEntity;
import com.vaskiv.model.ProgressEntity;
import com.vaskiv.model.ScheduleEntity;
import com.vaskiv.model.StudentsEntity;
import com.vaskiv.service.GroupsService;
import org.apache.logging.log4j.LogManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import static com.mysql.cj.jdbc.ConnectionGroupManager.getConnectionGroup;

public class AllTablesView {
     public void selectAllTable() throws SQLException, ClassNotFoundException {
        selectStudents();
        selectGroups();
        selectProgress();
        selectSchedule();
    }

    public void selectSchedule() throws SQLException {
       java.util.logging.Logger log = (java.util.logging.Logger) LogManager.getLogger("\nTable: Schedule");
        ScheduleDaoImpl scheduleService = new ScheduleDaoImpl();
        List<ScheduleEntity> schedule = scheduleService.findAll();
        for (ScheduleEntity entity : schedule) {
            System.out.println(entity);
        }
    }

    public void selectProgress() throws SQLException {
       java.util.logging.Logger log = (java.util.logging.Logger) LogManager.getLogger("\nTable: Progress");
        ProgressDaoImpl progressService = new ProgressDaoImpl();
        List<ProgressEntity> progress = progressService.findAll();
        for (ProgressEntity entity : progress) {
            System.out.println(entity);
        }
    }

    public void selectGroups() throws SQLException {
        java.util.logging.Logger log = (java.util.logging.Logger) LogManager.getLogger("\nTable: Groups");
        GroupsDaoImpl groupsService = new GroupsDaoImpl();
        List<GroupsEntity> groups = (List<GroupsEntity>) GroupsService.findAll();
        for (GroupsEntity entity : groups) {
            System.out.println(entity);
        }
    }

    public void selectStudents() throws SQLException {
       java.util.logging.Logger log = (java.util.logging.Logger) LogManager.getLogger("\nTable: Students");
        StudentsDaoImpl studentService = new StudentsDaoImpl();
        List<StudentsEntity> students = studentService.findAll();
        for (StudentsEntity entity : students) {
            System.out.println(entity);
        }
    }
/*
    public static void takeStructureOfDB() throws SQLException {
       Connection connection = ConnectionGroupManager.getConnectionGroup();
        MetaDataService metaDataService = new MetaDataService();
        List<TableMetaData> tables = metaDataService.getTablesStructure();
        Logger log = (Logger) LogManager.getLogger("TABLE OF DATABASE: " + connection.getCatalog());
        for (TableMetaData table: tables ) {
            System.out.println(table);
        }
    }*/
}


