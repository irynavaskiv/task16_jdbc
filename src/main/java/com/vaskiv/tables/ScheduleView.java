package com.vaskiv.tables;

import com.vaskiv.DAO.implementation.GroupsDaoImpl;
import com.vaskiv.DAO.implementation.ScheduleDaoImpl;
import com.vaskiv.model.GroupsEntity;
import com.vaskiv.model.ScheduleEntity;
import com.vaskiv.model.StudentsEntity;
import com.vaskiv.service.GroupsService;
import com.vaskiv.service.ScheduleService;
import com.vaskiv.service.StudentsService;
import org.apache.logging.log4j.LogManager;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Logger;

public class ScheduleView {
    private static Scanner INPUT ;

    public void findByAllSchedule() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Schedule: ");
        String idSchedule = INPUT.nextLine();
        ScheduleDaoImpl scheduleService = new ScheduleDaoImpl();
        ScheduleService.delete(idSchedule);
        log = (Logger) LogManager.getLogger("Deleted Schedule with id " + idSchedule);
    }
    public void findScheduleByID(String idSchedule) throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Schedule: ");
        idSchedule = INPUT.nextLine();
        ScheduleService scheduleService = new ScheduleService();
        ScheduleEntity entity = (ScheduleEntity) ScheduleService.findById(idSchedule);
        log = (Logger) LogManager.getLogger("Deleted Schedule with id " );
    }

    @SuppressWarnings("Duplicates")
    public void createSchedule() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Schedule: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input Schedule name: ");
        String semester = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input lecturer for Schedule: ");
        String lecturer = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input subject for Schedule: ");
        String subject = INPUT.nextLine();
        ScheduleEntity entity = new ScheduleEntity(id, semester, lecturer,subject);
        ScheduleService scheduleService = new ScheduleService();
        ScheduleService.create(entity);
        log = (Logger) LogManager.getLogger("Created new Schedule");
    }

    @SuppressWarnings("Duplicates")
    public void updateSchedule() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Schedule: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input Schedule name: ");
        String semester = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input lecturer for Schedule: ");
        String lecturer = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input subject for Schedule: ");
        String subject = INPUT.nextLine();
        ScheduleEntity entity = new ScheduleEntity(id, semester,lecturer,subject);
        ScheduleService scheduleService = new ScheduleService();
        int count = (int) ScheduleService.update(entity);
        log = (Logger) LogManager.getLogger("Updated Schedule with id " + id);
    }

    public void findScheduleByID() {
    }
}
