package com.vaskiv.tables;

import com.vaskiv.DAO.implementation.GroupsDaoImpl;
import com.vaskiv.model.GroupsEntity;
import com.vaskiv.service.GroupsService;
import org.apache.logging.log4j.LogManager;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Logger;

public class GroupsView {
    private static Scanner INPUT ;

    public void findByAllGroups() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Groups: ");
        String idGroups = INPUT.nextLine();
        GroupsDaoImpl cityService = new GroupsDaoImpl();
        GroupsService.delete(idGroups);
        log = (Logger) LogManager.getLogger("Deleted Speciality with id " + idGroups);
    }
    public void findGroupByID(String idGroup) throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Group: ");
        idGroup = INPUT.nextLine();
        GroupsService groupsService = new GroupsService();
        GroupsEntity entity = groupsService.findById(idGroup);
        log = (Logger) LogManager.getLogger("Deleted Groups with id " + idGroup);
    }

    @SuppressWarnings("Duplicates")
    public void createGroups() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Group: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input Groups name: ");
        String group_cipher = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input region for Group: ");
        String specialty = INPUT.nextLine();
        GroupsEntity entity = new GroupsEntity(id,group_cipher,specialty);
        GroupsService groupsService = new GroupsService();
        GroupsService.create(entity);
        log = (Logger) LogManager.getLogger("Created new Groups");
    }

    @SuppressWarnings("Duplicates")
    public void updateGroups() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Group: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input new name for Group: ");
        String group_cipher = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input new region for Group: ");
        String specialty = INPUT.nextLine();
        GroupsEntity entity = new GroupsEntity(id, group_cipher, specialty);
        GroupsService groupsService = new GroupsService();
        int count = groupsService.update(entity);
        log = (Logger) LogManager.getLogger("Updated Group with id " + id);
    }

    public void findGroupByID() {
    }
}
