package com.vaskiv.tables;

import com.vaskiv.DAO.implementation.ScheduleDaoImpl;
import com.vaskiv.DAO.implementation.StudentsDaoImpl;
import com.vaskiv.model.GroupsEntity;
import com.vaskiv.model.ScheduleEntity;
import com.vaskiv.model.StudentsEntity;
import com.vaskiv.service.ScheduleService;
import com.vaskiv.service.StudentsService;
import org.apache.logging.log4j.LogManager;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Logger;

public class StudentsView {
    private static Scanner INPUT ;

    public void findByAllStudents() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Students: ");
        String idStudents = INPUT.nextLine();
        StudentsDaoImpl scheduleService = new StudentsDaoImpl();
        StudentsService.delete(idStudents);
        log = (Logger) LogManager.getLogger("Deleted Students with id " + idStudents);
    }
    public void findStudentsByID(String idStudents) throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Students: ");
        idStudents = INPUT.nextLine();
        StudentsService studentsService = new StudentsService ();
        StudentsEntity entity = (StudentsEntity) StudentsService.findById(idStudents);
        log = (Logger) LogManager.getLogger("Deleted Students with id " );
    }

    @SuppressWarnings("Duplicates")
    public void createStudents() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Students: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input name_surname for Students: ");
        Integer name_surname = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input autobio for Students: ");
        Integer autobio = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input yearth_of_entry for Students: ");
        Integer yearth_of_entry = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input yeat_of_birth for Students: ");
        Integer yeat_of_birth = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input home_address for Students: ");
        Integer home_address = INPUT.nextInt();
        INPUT.nextLine();
        StudentsEntity entity = new StudentsEntity(id, name_surname,autobio,yearth_of_entry, yeat_of_birth,home_address);
        StudentsService studentsService = new StudentsService();
        StudentsService.create(entity);
        log = (Logger) LogManager.getLogger("Created new Students");
    }

    @SuppressWarnings("Duplicates")
    public void updateStudents() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Students: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input name_surname for Students: ");
        Integer name_surname = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input autobio for Students: ");
        Integer autobio = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input yearth_of_entry for Students: ");
        Integer yearth_of_entry = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input yeat_of_birth for Students: ");
        Integer yeat_of_birth = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input home_address for Students: ");
        Integer home_address = INPUT.nextInt();
        INPUT.nextLine();
        StudentsEntity entity = new StudentsEntity(id, name_surname,autobio,yearth_of_entry, yeat_of_birth,home_address);
        StudentsService studentsService = new StudentsService();
        int count = studentsService.update(entity);
        log = (Logger) LogManager.getLogger("Updated Students with id " + id);
    }

    public void findStudentsByID() {
            }
}
