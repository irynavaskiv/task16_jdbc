package com.vaskiv.tables;

import com.vaskiv.DAO.implementation.ProgressDaoImpl;
import com.vaskiv.model.ProgressEntity;
import com.vaskiv.service.ProgressService;
import org.apache.logging.log4j.LogManager;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Logger;

public class ProgressView {
    private static Scanner INPUT ;

    public void findByAllProgress() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Progress: ");
        String idProgress = INPUT.nextLine();
        ProgressDaoImpl progressService = new ProgressDaoImpl();
        ProgressService.delete(idProgress);
        log = (Logger) LogManager.getLogger("Deleted Progress with id " + idProgress);
    }
    public void findProgressByID(String idProgress) throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Progress: ");
        idProgress = INPUT.nextLine();
        ProgressService progressService = new ProgressService();
        ProgressEntity entity = progressService.findById(idProgress);
        log = (Logger) LogManager.getLogger("Deleted Speciality with id " );
    }

    @SuppressWarnings("Duplicates")
    public void createProgress() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Progress: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input Progress name: ");
        String rating = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input rating for Progress: ");
        String module_1 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input module_1 for Progress: ");
        String module_2 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input module_2 for Progress: ");
        String mark_100 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input mark_100 for Progress: ");
        String mark_5 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input mark_5 for Progress: ");
        String bursary = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input bursary for Progress: ");
        String specialty = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input specialty for Progress: ");
        ProgressEntity entity = new ProgressEntity();
        ProgressService progressService = new ProgressService();
        ProgressService.create(entity);
        log = (Logger) LogManager.getLogger("Created new Progress");
    }

    @SuppressWarnings("Duplicates")
    public void updateProgress() throws SQLException {
        Logger log = (Logger) LogManager.getLogger("Input ID for Progress: ");
        Integer id = INPUT.nextInt();
        INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input Progress name: ");
        String rating = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input rating for Progress: ");
        String module_1 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input module_1 for Progress: ");
        String module_2 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input module_2 for Progress: ");
        String mark_100 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input mark_100 for Progress: ");
        String mark_5 = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input mark_5 for Progress: ");
        String bursary = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input bursary for Progress: ");
        String specialty = INPUT.nextLine();
        log = (Logger) LogManager.getLogger("Input specialty for Progress: ");
        ProgressEntity entity = new ProgressEntity();
        ProgressEntity groupService = new ProgressEntity();
        log = (Logger) LogManager.getLogger("Updated Progress with id " + id);
    }

    public void findProgressByID() {
    }
}
