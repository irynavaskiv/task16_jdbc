package com.vaskiv.persistant;

import java.sql.*;

public class Connection {
    private static final String url = "jdbc:mysql://localhost:3306/sample?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String user = "root";
    private static final String password = "vaskiv";

    private static java.sql.Connection connection = null;

    private Connection() {
    }
    public static java.sql.Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                System.out.println("SQLException:" + e.getMessage());
                System.out.println("SQLState:" + e.getSQLState());
                System.out.println("VendorError:" + e.getErrorCode());
            }
        }
        return connection;
    }
 }
